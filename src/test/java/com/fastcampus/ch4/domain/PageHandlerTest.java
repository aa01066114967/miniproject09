package com.fastcampus.ch4.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/root-context.xml"})
public class PageHandlerTest {

    @Test
    public void Test(){
        PageHandler ph = new PageHandler(250, 1);
        // 이렇게 하면 페이지가 25개 나와야함
        ph.print();
        System.out.println("ph = " + ph);
        assertTrue( ph.getBeginPage() == 1);
        assertTrue(ph.getEndPage() == 10);
    }

    @Test
    public void Test2(){
        PageHandler ph = new PageHandler(250, 11);
        // 이렇게 하면 페이지가 25개 나와야함
        ph.print();
        System.out.println("ph = " + ph);
        assertTrue( ph.getBeginPage() == 11);
        assertTrue(ph.getEndPage() == 20);
    }

    @Test
    public void Test3(){
        PageHandler ph = new PageHandler(255, 25);
        // 이렇게 하면 페이지가 25개 나와야함
        ph.print();
        System.out.println("ph = " + ph);
        assertTrue( ph.getBeginPage() == 21);
        assertTrue(ph.getEndPage() == 26);
    }


}