package com.fastcampus.ch4.controller;

import com.fastcampus.ch4.domain.*;
import com.fastcampus.ch4.service.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.*;

import javax.servlet.http.*;
import java.time.*;
import java.util.*;

@Controller
@RequestMapping("/board")
public class BoardController {

    @Autowired
    BoardService boardService;

    @PostMapping("/modify")
    public String modify(BoardDto boardDto, HttpSession session, Model m , RedirectAttributes rattr
    , Integer page, Integer pageSize){
        String writer = (String)session.getAttribute("id");
        boardDto.setWriter(writer);

        try{
            int rowCnt = boardService.modify(boardDto);

            if (rowCnt != 1){
                throw new Exception("modify failed");
            }

            m.addAttribute("mode", "modify");
            rattr.addAttribute("msg", "modify_OK");
            m.addAttribute("page", page);
            m.addAttribute("pageSize", pageSize);

            return "board";
        } catch (Exception e){
            e.printStackTrace();
            m.addAttribute(boardDto);
            rattr.addAttribute("msg", "modify_ERR");
            return "board";
        }

    }

    @PostMapping("/write")
    public String write(BoardDto boardDto, HttpSession session, Model m , RedirectAttributes rattr){
        String writer = (String)session.getAttribute("id");
        boardDto.setWriter(writer);

        try{
            int rowCnt = boardService.write(boardDto);

            if (rowCnt != 1){
                throw new Exception("Write failed");
            }

            rattr.addAttribute("msg", "WRT_OK");

            return "redirect:/board/list";
        } catch (Exception e){
            e.printStackTrace();
            m.addAttribute(boardDto);
            rattr.addAttribute("msg", "WRT_ERR");
            return "board";
        }

    }

    @GetMapping("/write")
    public String write(Model m){
        m.addAttribute("mode", "new");

        return "board";
    }

    @PostMapping("/remove")
    public String remove(Integer bno,Integer page, Integer pageSize, Model m,
                         HttpSession session, RedirectAttributes rattr){

        String writer = (String) session.getAttribute("id");
        try {

            m.addAttribute("page", page);
            m.addAttribute("pageSize", pageSize);

            int rowCnt = boardService.remove(bno, writer);
            if(rowCnt != 1) {
                throw new Exception("board remove error");
            }
            rattr.addFlashAttribute("msg", "del_ok");
            return "redirect:/board/list?page=" + page + "&pageSize=" + pageSize;
            // 한 번 쓰고 지워버림
        } catch (Exception e) {
            e.printStackTrace();
            rattr.addFlashAttribute("msg", "DEL_ERR");
        }
        return "redirect:/board/list";
    }

    @GetMapping("/read")
    public String read(Integer bno,Integer page, Integer pageSize, Model m){
        try {
            BoardDto boardDto = boardService.read(bno);
            m.addAttribute(boardDto);
            m.addAttribute("page", page);
            m.addAttribute("pageSize", pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "board";
    }

    @GetMapping("/list")
    public String list(Integer page, Integer pageSize, Model m ,HttpServletRequest request) {
        if(!loginCheck(request))
            return "redirect:/login/login?toURL="+request.getRequestURL();  // 로그인을 안했으면 로그인 화면으로 이동

        if(page == null) page = 1;
        if(pageSize == null) pageSize = 10;

        try {

            int totalCnt = boardService.getCount();
            System.out.println("totalCnt = "+totalCnt);
            System.out.println("pageSize = " +pageSize);

            PageHandler pageHandler = new PageHandler(totalCnt, page, pageSize);


            Map map = new HashMap();
            map.put("offset", (page-1)*pageSize);
            map.put("pageSize", pageSize);

            List<BoardDto> list = boardService.getPage(map);
            m.addAttribute("list", list);
            m.addAttribute("ph", pageHandler);
            m.addAttribute("page", page);
            m.addAttribute("pageSize", pageSize);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return "boardList"; // 로그인을 한 상태이면, 게시판 화면으로 이동
    }

    private boolean loginCheck(HttpServletRequest request) {
        // 1. 세션을 얻어서
        HttpSession session = request.getSession();
        // 2. 세션에 id가 있는지 확인, 있으면 true를 반환
        return session.getAttribute("id")!=null;
    }
}