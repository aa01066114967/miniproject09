<%--
  Created by IntelliJ IDEA.
  User: sohyun
  Date: 2024-01-08
  Time: 오후 10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>fastcampus</title>
    <link rel="stylesheet" href="<c:url value='/css/menu.css'/>">
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
</head>
<body>
<div id="menu">
    <ul>
        <li id="logo">fastcampus</li>
        <li><a href="<c:url value='/'/>">Home</a></li>
        <li><a href="<c:url value='/board/list'/>">Board</a></li>
        <li><a href="<c:url value='/login/login'/>">login</a></li>
        <li><a href="<c:url value='/register/add'/>">Sign in</a></li>
        <li><a href=""><i class="fas fa-search small"></i></a></li>
    </ul>
</div>
<div style="text-align:center">
    <h2>게시물 ${mode == "new" ? "글쓰기" : "읽기"}</h2>
    <form action="" id="forms">
        <input type="hidden" type="text" name="bno" value="${boardDto.bno}" >
        <input type="text" name="title"  value="${boardDto.title}" ${mode == "new" ? '' : 'readonly = "readonly"'}>
        <textarea name="content" id="" cols="30" rows="10" ${mode == "new" ? '' : 'readonly = "readonly"'}>${boardDto.content}</textarea>
        <c:choose>
            <c:when test="${mode eq 'new'}">
                <button type="button" id="writeBtn" class="btn">등록</button>
            </c:when>
        </c:choose>
        <button type="button" id="modifyBtn" class="btn">수정</button>
        <button type="button" id="removeBtn" class="btn">삭제</button>
        <button type="button" id="listBtn" class="btn">
            목록
        </button>
    </form>

</div>

<script>

    let msg = "${msg}";
    if(msg == "WRT_ERR") {
        alert('등록이 실패되었습니다.');
    }

    $(document).ready(function (){

        $('#listBtn').on("click", function (){
            location.href = "<c:url value='/board/list'/>?page=${page}&pageSize=${pageSize}"
        });


        $('#modifyBtn').on("click", function (){
            let form = $("#forms");
            let isReadOnly = $("input[name=title]").attr('readonly');

            if (isReadOnly == 'readonly'){
                $("input[name=title]").attr('readonly', false); // 제목
                $("textarea").attr('readonly', false); // content
                $("#modifyBtn").html("등록");
                $("h2").html("게시물 수정");
                return;
            }

            form.attr("action","<c:url value='/board/modify'/>?page=${page}&pageSize=${pageSize}");
            form.attr("method","post");
            form.submit();
        });

        $('#writeBtn').on("click", function (){
            let form = $("#forms");
            form.attr("action","<c:url value='/board/write'/>");
            form.attr("method","post");
            form.submit();
        });

        $('#removeBtn').on("click", function (){
            let form = $("#forms");
            form.attr("action","<c:url value='/board/remove'/>?page=${page}&pageSize=${pageSize}");
            form.attr("method","post");
            form.submit();
            alert('삭제')
        });

        });

</script>
</body>
</html>
